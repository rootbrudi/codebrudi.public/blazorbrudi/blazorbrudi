namespace Blazorbrudi.Contract;

public abstract class CodebrudiComponentBase : ComponentBase
{
    protected virtual Task OnAfterFirstRenderAsync() { return Task.CompletedTask; }
    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);
        if (firstRender) await OnAfterFirstRenderAsync();
    }

    protected virtual void OnAfterFirstRender() {}
    protected override void OnAfterRender(bool firstRender)
    {
        base.OnAfterRender(firstRender);
        if (firstRender) OnAfterFirstRender();
    }
}
