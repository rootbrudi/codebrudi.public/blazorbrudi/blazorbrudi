using System.ComponentModel;

namespace Blazorbrudi.Contract;

public abstract class CodebrudiMvvmComponentBase<TViewModel> : CodebrudiComponentBase
    where TViewModel : INotifyPropertyChanged
{
    private TViewModel? _viewModel;

    [Inject]
    public TViewModel? ViewModel
    {
        get => _viewModel;
        set
        {
            if (_viewModel is not null)
            {
                _viewModel.PropertyChanged -= ViewModelOnPropertyChanged;
            }

            _viewModel = value;

            if (_viewModel is not null)
            {
                _viewModel.PropertyChanged += ViewModelOnPropertyChanged;
            }
        }
    }

    private void ViewModelOnPropertyChanged(object? sender, PropertyChangedEventArgs e)
    {
        InvokeAsync(StateHasChanged);
    }
}
