using System.ComponentModel;

namespace Blazorbrudi.Contract;

public interface IPageComponentViewModel : INotifyPropertyChanged
{
    /// <summary>
    /// Browser tab title.
    /// </summary>
    public string PageTitle { get; }
}
