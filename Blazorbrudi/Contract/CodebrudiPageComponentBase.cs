using System.ComponentModel;

namespace Blazorbrudi.Contract;

public abstract class CodebrudiPageComponentBase<TViewModel> : CodebrudiMvvmComponentBase<TViewModel>
    where TViewModel : IPageComponentViewModel
{
    /// <summary>
    /// Browser tab title.
    /// </summary>
    public string PageTitle { get; } = string.Empty;
}
