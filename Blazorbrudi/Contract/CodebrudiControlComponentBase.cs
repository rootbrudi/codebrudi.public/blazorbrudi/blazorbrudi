using System.ComponentModel;

namespace Blazorbrudi.Contract;

public abstract class CodebrudiControlComponentBase<TViewModel> : CodebrudiMvvmComponentBase<TViewModel>
    where TViewModel : INotifyPropertyChanged
{
    /// <summary>
    /// Unique identifier for use on page.
    /// </summary>
    [Parameter] public string Id { get; set; } = string.Empty;
    /// <summary>
    /// Custom stuff here.
    /// </summary>
    [Parameter] public object? Tag { get; set; }
}
