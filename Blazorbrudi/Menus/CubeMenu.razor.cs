namespace Blazorbrudi.Menus;

public partial class CubeMenu : CodebrudiComponentBase
{
    private const string ShowFront = "";
    private const string ShowBack = "cube--show-back";
    private const string ShowLeft = "cube--show-left";
    private const string ShowRight = "cube--show-right";
    private const string ShowTop = "cube--show-top";
    private const string ShowBottom = "cube--show-bottom";

    [Parameter] public RenderFragment? Front { get; set; }
    [Parameter] public RenderFragment? Back { get; set; }
    [Parameter] public RenderFragment? Left { get; set; }
    [Parameter] public RenderFragment? Right { get; set; }
    [Parameter] public RenderFragment? Top { get; set; }
    [Parameter] public RenderFragment? Bottom { get; set; }

    private string Css { get; set; } = ShowFront;
    
    /// <summary>
    /// Rotates cube to selected side.
    /// </summary>
    /// <param name="side">Side to show.</param>
    /// <exception cref="ArgumentOutOfRangeException">If side unknown.</exception>
    public void ShowSide(CubeSide side)
    {
        Css = side switch
        {
            CubeSide.Front => ShowFront,
            CubeSide.Back => ShowBack,
            CubeSide.Left => ShowLeft,
            CubeSide.Right => ShowRight,
            CubeSide.Top => ShowTop,
            CubeSide.Bottom => ShowBottom,
            _ => throw new ArgumentOutOfRangeException(nameof(side), side, "Side unknown!")
        };
    }
}
