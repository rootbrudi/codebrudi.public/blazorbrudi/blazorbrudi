namespace Blazorbrudi.Menus;

/// <summary>
/// Sides of cube.
/// </summary>
public enum CubeSide
{
    Front,
    Back,
    Left,
    Right,
    Top,
    Bottom
}
